



OUTPUTDIR = os.environ.get("OUTPUTDIR")
SAMPLES = os.environ.get("SAMPLES").split("\n")
REFERENCES = os.environ.get("REFERENCES").split(" ")
GFFS = os.environ.get("GFFS").split(" ")

include:
    "rules/trimming.rules"
include:
    "rules/qc.rules"
include:
    "rules/reference_genomes.rules"
include:
    "rules/mapping.rules"
include:
    "rules/analysis.rules"


workdir: 
    OUTPUTDIR



rule all:
    input:
      expand("Preprocessed/{sample}/R1.trimmed.fastq.gz", sample=SAMPLES),
       expand("QC_raw/{sample}/R2_fastqc.zip", sample=SAMPLES),
       expand("Preprocessed/{sample}/SE.trimmed.fastq.gz", sample=SAMPLES),
       expand("QC_trimmed/{sample}/SE.trimmed_fastqc.zip",sample=SAMPLES),
       "References/references_concat.fa",
       "References/references_concat.fa.1.bt2",
       "References/annotations_concat.gff",
       expand("Mappings/bowtie2/{sample}.reads_x_references_concat.aligned.sorted.bam",sample=SAMPLES),    
       expand("Featurecounts/{sample}.reads_x_references_concat.featuresall.tsv",sample=SAMPLES),
    output:
        "done.txt"
    shell:
       "touch {output[0]}"


